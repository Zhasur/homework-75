const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const router = express.Router();

router.post('/', (req, res) => {
    console.log(req.body);
    const decipher = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({"decoded": decipher});
});

// export default router
module.exports = router;