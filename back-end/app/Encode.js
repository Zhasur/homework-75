const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const router = express.Router();

router.post('/', (req, res) => {
    console.log(req.body);
    const cipher = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encoded: cipher});
});

// export default router
module.exports = router;