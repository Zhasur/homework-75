const express = require('express');
const cors = require('cors');
const Encode = require('./app/Encode');
const Decode = require('./app/Decode');
const app = express();

app.use(express.json());
app.use(cors());

const port = 8020;

app.use('/encode', Encode);
app.use('/decode', Decode);

app.listen(port, () => {
    console.log(`Server started on ${port} port`)
});