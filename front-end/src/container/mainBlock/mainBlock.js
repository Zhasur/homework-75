import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {decodeMessage, encodeMessage, inputCipher, inputDecipher, inputPassword} from "../../store/actions/actions";
import {connect} from "react-redux";

class MainBlock extends Component {

    render() {
        return (
            <Form>
                <FormGroup row>
                    <Label sm={2} for="decoded">Decoded message</Label>
                    <Col sm={5}>
                        <Input
                            style={{minHeight: "100px"}}
                            type="textarea" required
                            name="decipher" id="decoded"
                            value={this.props.cipher}
                            onChange={this.props.inputCipher}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="password">Password</Label>
                    <Col sm={3}>
                        <Input
                            type="text" required min="0"
                            name="password" id="password"
                            value={this.props.password}
                            onChange={(event) => this.props.inputPassword(event.target.value)}
                        />
                    </Col>
                    <Button
                        style={{marginRight: "4px"}}
                        color="primary"
                        onClick={() => this.props.encodeMessage(this.props.cipher, this.props.password)}
                    >
                        <i className="fas fa-arrow-circle-down"/>
                    </Button>
                    <Button
                        color="primary"
                        onClick={() => this.props.decodeMessage(this.props.decipher, this.props.password)}

                    >
                        <i className="fas fa-arrow-circle-up"/>
                    </Button>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="encoded">Encoded message</Label>
                    <Col sm={5}>
                        <Input
                            style={{minHeight: "100px"}}
                            type="textarea" required
                            name="cipher" id="encoded"
                            value={this.props.decipher}
                            onChange={(event) => this.props.inputDecipher(event.target.value)}
                        />
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => ({
    cipher: state.cipher,
    decipher: state.decipher,
    password: state.password
});

const mapDispatchToProps = dispatch => ({
    encodeMessage: (message, password) => dispatch(encodeMessage(message, password)),
    decodeMessage: (message, password) => dispatch(decodeMessage(message, password)),
    inputPassword: (text) => dispatch(inputPassword(text)),
    inputCipher: (event) => dispatch(inputCipher(event)),
    inputDecipher: (event) => dispatch(inputDecipher(event)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MainBlock);