import {CIPHER, DECIPHER, DECODE_SUCCESS, ENCODE_SUCCESS, PASSWORD} from "../actions/actions";

const initialState = {
    cipher: '',
    decipher: '',
    password: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ENCODE_SUCCESS:
            return {...state, decipher: action.cipher};
        case DECODE_SUCCESS:
            return {...state, cipher: action.decipher};
        case PASSWORD:
            return {...state, password: action.text};
        case CIPHER:
            return {...state, cipher: action.encode};
        case DECIPHER:
            return {...state, decipher: action.decode};
        default:
            return state;
    }
};

export default reducer;