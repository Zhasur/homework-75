import axios from '../../axios-encode'

export const ENCODE_SUCCESS = 'ENCODE_SUCCESS';
export const DECODE_SUCCESS = 'DECODE_SUCCESS ';
export const PASSWORD = 'PASSWORD';
export const CIPHER = 'CIPHER';
export const DECIPHER = 'DECIPHER';

export const encodeSuccess = cipher => ({type: ENCODE_SUCCESS, cipher});
export const decodeSuccess = decipher => ({type: DECODE_SUCCESS, decipher});

export const inputPassword = text => ({type: PASSWORD, text});
export const inputCipher = event => ({type: CIPHER, encode: event.target.value});
export const inputDecipher = decode => ({type: DECIPHER, decode});

export const encodeMessage = (message, password) => {
    console.log(password);
    const data = {message, password};
    return dispatch => {
        return axios.post('/encode', data).then(response => {
            console.log(response.data);
            dispatch(encodeSuccess(response.data.encoded))
        })
    }
};

export const decodeMessage = (message, password) => {
    const data = {message, password};
    console.log(data, 'MESS');
    return dispatch => {
        return axios.post('/decode', data).then(response => {
            console.log(response.data);
            dispatch(decodeSuccess(response.data.decoded))
        })
    }
};