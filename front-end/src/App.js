import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import MainBlock from "./container/mainBlock/mainBlock";
import {Container} from "reactstrap";

class App extends Component {
  render() {
    return (
        <Container style={{marginTop: "40px"}}>
          <Switch>
              <Route path="/" exact component={MainBlock} />
          </Switch>
        </Container>
    );
  }
}

export default App;
