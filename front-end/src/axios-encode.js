import axios from 'axios'

const instance = axios.create({
    baseURL: 'http://localhost:8020'
});

export default instance